FROM --platform=$BUILDPLATFORM google/cloud-sdk:405.0.0-alpine

ENV GOOSE_VERSION=3.7.0
ARG GOOSE_LINUX_X86=goose_linux_x86_64
ARG GOOSE_LINUX_ARM64=goose_linux_arm64
ARG GOOSE_DARWIN_ARM64=goose_darwin_arm64
WORKDIR /local

ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /log

RUN if [ "$TARGETPLATFORM" == "linux/amd64" ]; then echo "${GOOSE_LINUX_X86}">image ; fi
RUN if [ "$TARGETPLATFORM" == "linux/arm64" ]; then echo "${GOOSE_LINUX_ARM64}">image ; fi
RUN if [ "$TARGETPLATFORM" == "darwin/arm64" ]; then echo "${GOOSE_DARWIN_ARM64}">image ; fi
RUN apk --update --no-cache add postgresql-client
RUN apk --no-cache add jq curl

RUN echo "downloading https://github.com/pressly/goose/releases/download/v${GOOSE_VERSION}/$(cat image)"
RUN URL=$(cat image) && curl -ksSL https://github.com/pressly/goose/releases/download/v${GOOSE_VERSION}/${URL}  -o goose
RUN chmod +x goose
RUN mv ./goose /usr/local/bin/goose

RUN goose --version
RUN pg_dump --version
RUN gcloud --version
CMD ["pg_dump"]
